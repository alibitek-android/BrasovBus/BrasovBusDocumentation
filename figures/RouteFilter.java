private class RouteFilter extends Filter {

@Override
protected FilterResults performFiltering(CharSequence charSequence) {
	FilterResults results = new FilterResults();

	if (mOriginalValues == null) {
		synchronized (mLock) {
			mOriginalValues = new ArrayList<Route>(routeList);
		}
	}

	if (charSequence == null || charSequence.length() == 0) {
		ArrayList<Route> list;
		synchronized (mLock) {
			list = new ArrayList<Route>(mOriginalValues);
		}
		results.values = list;
		results.count = list.size();
	} else {
		String prefixString = charSequence.toString().toLowerCase();

		ArrayList<Route> values;
		synchronized (mLock) {
			values = new ArrayList<Route>(mOriginalValues);
		}

		final int count = values.size();
		final ArrayList<Route> newValues = new ArrayList<Route>();

		for (int i = 0; i < count; i++) {
			final Route value = values.get(i);

			final String valueText = value.getRoute_long_name()
					.toLowerCase();

			if (valueText.contains(prefixString.toLowerCase())) {
				newValues.add(value);
			} 
			else 
			{
			 final String[] words = valueText.split(" ");
			 final int wordCount = words.length;

			 for (int k = 0; k < wordCount; k++) {
			  if (words[k].toLowerCase().contains(
					prefixString.toLowerCase())) {
					  newValues.add(value);
					  break;
					}
				}
			}
		}

		results.values = newValues;
		results.count = newValues.size();
	}

	return results;
}

@SuppressWarnings("unchecked")
@Override
protected void publishResults(CharSequence charSequence,
		FilterResults filterResults) {
	routeList = (List<Route>) filterResults.values;
	if (filterResults.count > 0) {
		notifyDataSetChanged();
	} else {
		notifyDataSetInvalidated();
	}
}
}